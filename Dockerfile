FROM node:latest

WORKDIR nomination-app
COPY . .

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

RUN npm install && npm run build

CMD /wait && npm run prod-start