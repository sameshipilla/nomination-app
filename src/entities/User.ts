export interface User {
    name: string
    email: string
    password: string
    dateOfRegistration: Date | null
}