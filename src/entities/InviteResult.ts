import {Question} from "./Question";

export interface InviteResult {
    gameRemainingTime: number,
    questionRemainingTime: number,
    targetQuestion: Question
}