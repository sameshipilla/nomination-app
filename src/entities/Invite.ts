export interface Invite {
    trackerId: string
    questionTrackerId: string
    targetUserEmail: string
    nextInviteTrackId?: string
    answer1?: string
    answer2?: string
    answer3?: string
    answered?: boolean
    attempted?: boolean
    expiryDate?: Date
}