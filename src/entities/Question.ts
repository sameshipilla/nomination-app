export interface Question {
    initiatorEmail: string
    questionText: string
    expectedNumAnswers: number
    questionTimer: number
    gameTimer: number
    trackerId: string
    firstInviteTrackId: string
    expiryDate?: Date
}