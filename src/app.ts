import express from 'express';
import {init, userCollection} from './init';
import { authRouter } from './routers/auth_router';
import { questionRouter } from './routers/question_router';
import {generalRouter} from "./routers/general_router";
import bodyParser from 'body-parser';
import cookieParser from "cookie-parser";
import session from "express-session";
import {Strategy as LocalStrategy} from 'passport-local';
import { compare } from 'bcrypt';
import passport from "passport";
import {User} from "./entities/User";
import {add} from "./service/online";
import {initNotifier} from "./service/invite_notifier";


const app = express();

app.set('view engine', 'ejs');

/**
 * Configure passport
 */
passport.serializeUser((user: User, done) => {
    done(null, user.email);
});

passport.deserializeUser(async (id: string, done) => {
    const user = await userCollection.findOne({email: id});
    done(null, user);
});

passport.use(new LocalStrategy((async (username, password, done) => {
    const targetUser: User = await userCollection.findOne({email: username});
    if(!targetUser){
        return done(null, false);
    }

    const matches = await compare(password, targetUser.password);
    if(!matches){
        return done(null, false);
    }

    return done(null, targetUser);
})));

app.use(session({
    secret: '545a0fe4c3a5f7ed53f94e0bf5172fda59aff8530150300a28ca129181aa24970eb562b9947e7528',
    saveUninitialized: false,
    resave: false
}));


app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser())

app.use(authRouter);
app.use(express.static('public'));
app.use(isLoggedIn);
app.use(activityTracker);
app.use(questionRouter);
app.use(generalRouter);


init().then(() => {

    const server = app.listen(process.env.PORT, () => {
         console.log(`Started server on port: ${process.env.PORT}`);
    });
    initNotifier(server);
});

// activity tracking of a user.
async function activityTracker(req, res, next){
    await add(req.user['email']);
    res.cookie('whomai', req.user['email']);
    next();
}

//route middleware to ensure user is logged in
export function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }

    res.redirect('/login?message=Access Denied. Please Login');
}