export interface EmailRecipient {
    to: string,
    subject: string,
    message: string
}