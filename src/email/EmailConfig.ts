export interface EmailConfig {
    actionLink: string
    actionLinkText: string
    introductionText: string
}