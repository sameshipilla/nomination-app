/**
 * Creates an email based on the email template beside it.
 */
import {EmailConfig} from "./EmailConfig";
import fs from 'fs';
import {ACTION_LINK_KEY, ACTION_LINK_TEXT_KEY, INTRODUCTION_TEXT_KEY} from "./email_constants";

export function createEmail(emailConfig: EmailConfig): Promise<string>{
    return new Promise<string>((resolve, reject) => {
        fs.readFile("email_template.html", ((err, data) => {
            if(err){
                reject(err);
            } else {
                const emailTemplate = data.toString("utf-8");
                const email = emailTemplate.replace(INTRODUCTION_TEXT_KEY, emailConfig.introductionText)
                    .replace(ACTION_LINK_TEXT_KEY, emailConfig.actionLinkText)
                    .replace(ACTION_LINK_KEY, emailConfig.actionLink)

                resolve(email);
            }
        }));
    });
}