import sendgrid from '@sendgrid/mail';
import {EmailRecipient} from "./EmailRecipient";

export async function sendEmail(emailRecipient: EmailRecipient) {
    sendgrid.setApiKey(process.env.SENDGRID_API_KEY);

    const message  = {
        to: emailRecipient.to,
        subject: emailRecipient.subject,
        from: 'noreply@nominations',
        html: emailRecipient.message
    };

    await sendgrid.send(message);
}