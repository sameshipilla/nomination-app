import {Router} from "express";
import {userCollection} from "../init";
import { hash, compare } from "bcrypt";
import {User} from "../entities/User";
import {remove} from "../service/online";


export const generalRouter = Router();

generalRouter.get('/update_password', (req, res) => {
    const { message, status } = req.query;
    res.render('update_password', {message, status, user: req.user});
});

generalRouter.post('/update_password', async (req, res) => {
    const { oldPassword, newPassword, confirmNewPassword } = req.body;
    if(!oldPassword || !newPassword || !confirmNewPassword){
        return res.redirect('/update_password?message=All fields are required')
    }

    if(newPassword !== confirmNewPassword){
        return res.redirect('/update_password?message=Passwords do not match!');
    }

    if(newPassword.length < 6){
        return res.redirect('/update_password?message=New password should be 6 characters or more.');
    }

    const user: User = req.user as User;

    if(!await compare(oldPassword, user.password)){
        return res.redirect('/update_password?message=Invalid old password');
    }

    await userCollection.updateOne({email: user.email}, {$set: {password: await hash(newPassword, 10)}});

    return res.redirect('/update_password?message=Updated password successfully&status=success');
});

generalRouter.get('/logout', async (req, res) => {
    await remove(req.user['email']);
    req.logout();
    res.redirect('/login?message=Logged you out successfully!&status=success');
});
