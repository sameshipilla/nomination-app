import {Router} from "express";
import {getAsync, setAsync, setexAsync, userCollection} from "../init";
import {User} from "../entities/User";
import bcrypt from 'bcrypt';
import {randomBytes} from 'crypto';
import {EmailConfig} from "../email/EmailConfig";
import {createEmail} from "../email/create_email";
import {EmailRecipient} from "../email/EmailRecipient";
import {sendEmail} from "../email/send_email";
import passport from "passport";

export const authRouter = Router();

authRouter.get('/login', (req, res) => {
    const {status, message} = req.query;
    res.render('login', {message, status});
});

authRouter.get('/register', (req, res) => {
    const {status, message} = req.query;

    res.render('register', {message, status});
});

authRouter.get('/reset_password', (req, res) => {
    const {status, message} = req.query;
    res.render('start_pass_reset', {message, status});
});

authRouter.get('/confirm_password_reset', (req, res) => {
    const  { message, status, t }  = req.query;
    res.render('confirm_password_reset', {message, status, t});
});

authRouter.post('/confirm_password_reset', async (req, res) => {
    const { confirmNewPassword, newPassword, t} = req.body;

    if(!t){
        return res.redirect('/reset_password?message=The link used has expired or is invalid. Try again');
    }

    if(!confirmNewPassword || !newPassword){
        return res.redirect(`/confirm_password_reset?t=${t}&message=All fields are required`);
    }

    if(confirmNewPassword !== newPassword){
        return res.redirect(`/confirm_password_reset?t=${t}&message=Passwords do not match`);
    }

    if(newPassword.length < 6){
        return res.redirect(`/confirm_password_reset?t=${t}&message=New Password should be 6 characters or more.`);
    }

    const userData = await getAsync(t);
    if(!userData){
        return res.redirect('/reset_password?message=The link used has expired or is invalid. Try again');
    }

    const user: User = JSON.parse(userData);

    setImmediate(async () => {
        await userCollection.updateOne({email: user.email}, {$set: {password: await bcrypt.hash(newPassword, 10)}});
    });

    res.redirect('/login?status=success&message=Updated password successfully!');
});

authRouter.post('/register', async (req, res) => {
    const {email, password, name } = req.body;

    if(!name || name.trim().length < 2){
        return res.redirect('/register?status=error&message=Name should be greater than 2 characters');
    }

    if (!validateEmail(email)) {
        return res.redirect('/register?status=error&message=Invalid email is required');
    }

    if (!password) {
        return res.redirect('/register?status=error&message=Password is required');
    }

    const userExists = await userCollection.findOne({email});
    if (userExists) {
        return res.redirect('/register?status=error&message=Email is already taken');
    }

    const user: User = {
        email: email.trim(),
        password: await bcrypt.hash(password, 10),
        dateOfRegistration: null,
        name: name.trim()
    }

    const userData = JSON.stringify(user);
    const userKey = randomBytes(100).toString('hex');
    await setexAsync(userKey, (+process.env.REGISTRATION_LINK_EXPIRATION_IN_MINUTES * 60), userData);

    setImmediate(async () => {
        const emailConfig: EmailConfig = {
            actionLink: `${process.env.BASE_URL}/confirm_registration?t=${userKey}`,
            actionLinkText: 'Confirm Email Address',
            introductionText: `Click the link below to confirm your email. The link expires in ${process.env.REGISTRATION_LINK_EXPIRATION_IN_MINUTES} minutes.`
        };

        const emailMsg = await createEmail(emailConfig);

        const emailRecipient: EmailRecipient = {
            to: user.email,
            subject: 'Confirm Email Address',
            message: emailMsg
        };

        await sendEmail(emailRecipient);
    });

    return res.render('register', {
        status: 'success',
        message: `A confirmation email has been sent to: ${user.email}`
    });
});


authRouter.get('/confirm_registration', async (req, res) => {
    const { t } = req.query;
    if(!t){
        return res.render('register', {status: 'warning', message: 'Invalid link or link has expired. Please try again.'});
    }

    const userData = await getAsync(t);
    if(!userData){
        return res.render('register', {status: 'warning', message: 'Invalid link or link has expired. Please try again.'});
    }

    const user: User = JSON.parse(userData);
    const userExists = await userCollection.findOne({email: user.email});
    if(userExists){
        return res.render('register', {status: 'error', message: 'You already have an account with us. Kindly log in'});
    }

    user.dateOfRegistration = new Date();
    await userCollection.insertOne(user);

    res.render('login', {message: 'Registration successful! Please log in', status: 'success'});
});

authRouter.post('/reset_password', async (req, res) => {
    let { email } = req.body;
    if(!email){
        return res.redirect(`/reset_password?message=Email is required`);
    }
    email = email.trim();

    const targetUser = await userCollection.findOne({email});
    if(!targetUser){
        return res.redirect('/reset_password?status=success&message=If there is an account associated with this email, a password reset ' +
            'link has been sent to: ' + email);
    }

    setImmediate(async () => {
        const userKey = randomBytes(100).toString('hex');

        const emailConfig: EmailConfig = {
            introductionText: `Click the link below to reset your password. The link expires in ${process.env.RESET_PASSWORD_LINK_EXPIRATION_IN_MINUTES} minutes.`,
            actionLinkText: 'Reset Password',
            actionLink: `${process.env.BASE_URL}/confirm_password_reset?t=${userKey}`
        }

        await setexAsync(userKey, (+process.env.RESET_PASSWORD_LINK_EXPIRATION_IN_MINUTES * 60), JSON.stringify(targetUser));
        const emailMsg = await createEmail(emailConfig);

        const emailRecipient: EmailRecipient = {
            message: emailMsg,
            subject: 'Reset Password',
            to: email
        }

        await sendEmail(emailRecipient);
    });

    return res.redirect('/reset_password?status=success&message=If there is an account associated with this email, a password reset ' +
        'link has been sent to: ' + email);
});

authRouter.post('/login', passport.authenticate('local', {
    session: true,
    failureRedirect: '/login?message=Invalid username or password',
    successRedirect: '/'
}));

function validateEmail(email) {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
}