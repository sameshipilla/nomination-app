import {Router} from "express";
import {Question} from "../entities/Question";
import {getAsync, inviteCollection, questionCollection, setexAsync, userCollection} from "../init";
import {randomBytes} from 'crypto';
import {getOnlineUsers, getReInvitableOnlineUsers} from "../service/online";
import {
    acceptInvite,
    answerQuestion,
    getAnsweredInvites, getAnsweredQuestions,
    getPendingInvites,
    nominateUser,
    renominateUser
} from "../service/invite";

export const questionRouter = Router();


questionRouter.get('/online_users', async (req, res, next) => {
    const { qnToken } = req.query;
    if(!qnToken)
        return res.redirect('/ask_question?message=You need to ask a question before accessing the online users')

    const qnData = await getAsync(qnToken);
    if(!qnData)
        return res.redirect('/ask_question?message=The currently asked question you asked has timed out. Please try again.');

    const ids = await getOnlineUsers();

    const targetIndex = ids.indexOf(req.user['email']);
    ids.splice(targetIndex, 1);

    let users = await userCollection.find({email: {$in: ids}}).toArray();
    users = users.map(user => {
        const username = user.email.split('@')[0]
        return {
            name: user.name,
            username,
            email: user.email
        }
    });

    res.render('online_users', {users, qnToken});
});


questionRouter.get('/view_answers', async (req, res) => {
    const answeredQuestions = await getAnsweredQuestions(req.user['email']);
    res.render('view_answers', {user: req.user, answeredQuestions});
});

questionRouter.get('/', async (req, res) => {
    const {message, status} = req.query;

    const updatedInvites = await getPendingInvites(req.user['email']);

    res.render('home', {user: req.user, message, status, pendingInvites: updatedInvites});
});

questionRouter.get('/ask_question', async (req, res) => {
    const { qnToken, message, status } = req.query;

    let tempQuestion: Question = {
        trackerId: "",
        expectedNumAnswers: 1,
        firstInviteTrackId: "",
        gameTimer: 0,
        initiatorEmail: "",
        questionText: "",
        questionTimer: 0
    }

    if(qnToken && qnToken.toString().trim()){
        const tempQnData = await getAsync(qnToken);
        if(tempQnData) {
            tempQuestion = JSON.parse(tempQnData);
        }
    }

    res.render('ask_question', {tempQuestion, user: req.user, message, status});
});

questionRouter.post('/ask_question', async (req, res) => {
    const question: Question = req.body as Question;
    // todo: validate question here.

    const qnToken = `tmp.${randomBytes(100).toString('hex')}`;
    await setexAsync(qnToken, 60 * 30, JSON.stringify(question));

    res.redirect(`/nominate_user?qnToken=${qnToken}`);
});

questionRouter.get('/nominate_user', async (req, res) => {
    const { qnToken } = req.query;
    if(!qnToken)
        return res.redirect('/ask_question');

    const targetQnJSON = await getAsync(qnToken);
    if(!targetQnJSON)
        return res.redirect('/ask_question');

    res.render('nominate_user', {user: req.user, qnToken});
});

questionRouter.post('/nominate_user', async (req, res) => {
    const { qnToken, inviteEmail} = req.body;
    try {
        const resultLink = await nominateUser(qnToken, inviteEmail, req.user['email']);
        return res.redirect(resultLink);
    } catch (e) {
        handleServerError(e, res);
    }
});

questionRouter.get('/accept_invite', async(req, res) => {
    const { trackerId } = req.query;

    try {
        const renderData: any = await acceptInvite(trackerId as string);
        renderData.user = req.user;
        renderData.trackerId = trackerId;
        res.render('answer_question', renderData)
    } catch (e) {
        handleServerError(e, res);
    }
});

questionRouter.post('/answer_question', async (req, res) => {
    const { answer1, answer2, answer3, trackerId, questionsNum } = req.body;
    try {
        const resultLink = await answerQuestion(answer1, answer2, answer3, trackerId, questionsNum);
        res.redirect(resultLink);
    } catch (e) {
        handleServerError(e, res);
    }
});

questionRouter.get('/renominate_user', (req, res) => {
    const { prevTrackerId } = req.query;
    res.render('renominate_user', {prevTrackerId});
});

questionRouter.get('/renominate_online_users', async (req, res) => {
    const { prevTrackerId } = req.query;
    if(!prevTrackerId)
        return res.redirect('/?message=Invalid URL. Try again. No Prev Tracker id');

    const prevTrackerFilter = {
        trackerId: prevTrackerId,
        attempted: true
    }
    const prevInvite = await inviteCollection.findOne(prevTrackerFilter);
    if(!prevInvite){
        return res.redirect('/?message=Invalid URL. Try again.')
    }

    const questionFilter = {
        trackerId: prevInvite.questionTrackerId,
        expiryDate: {$gte: new Date()}
    }

    const targetQuestion: Question = await questionCollection.findOne(questionFilter);
    if(!targetQuestion){
        return res.redirect(`/?message=The game has already expired.`);
    }

    const users = await getReInvitableOnlineUsers(prevInvite, req.user['email']);
   res.render('renominate_online_users', {users, prevTrackerId});
});

questionRouter.post('/renominate_user', async (req, res) => {
    const { inviteEmail, prevTrackerId } = req.body;
    try {
        const resultLink = await renominateUser(inviteEmail, prevTrackerId, req.user['email']);
        res.redirect(resultLink);
    } catch (e) {
        handleServerError(e, res);
    }
});

questionRouter.get('/single_question_answers', async (req, res) => {
    const { qnTrackerId } = req.query;
    if(!qnTrackerId)
        return res.redirect('/view_answers?message=Select a question and try again.');

    const targetQuestion: Question = await questionCollection.findOne({trackerId: qnTrackerId});
    if(!targetQuestion){
        return res.redirect('/view_answers?message=Select a question and try again.');
    }

    const answeredInvites = await getAnsweredInvites(targetQuestion);

    return res.render('single_question_answers', {answeredInvites, targetQuestion});
});

export function handleServerError(e, res){
    if(e.toString().startsWith('/')){
        return res.redirect(e.toString());
    }
    res.redirect('/?message=The server could not fulfill your request');
}