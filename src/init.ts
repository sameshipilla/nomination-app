import { config } from 'dotenv';
import {MongoClient} from "mongodb";
import {RedisClient} from "redis";
import { promisify } from 'util';
import {initOnline} from "./service/online";

// mongo collections
export let userCollection;
export let questionCollection;
export let inviteCollection;

// redis commands
export let getAsync;
export let setAsync;
export let setexAsync;


export async function init(){
    config();
    
    const redisClient = new RedisClient({host: process.env.REDIS_HOST});
    getAsync = promisify(redisClient.get).bind(redisClient);
    setAsync = promisify(redisClient.set).bind(redisClient);
    setexAsync = promisify(redisClient.setex).bind(redisClient);

    initOnline(redisClient);

    const mongoClient = new MongoClient(process.env.MONGO_DB_URL);
    await mongoClient.connect();

    userCollection = mongoClient.db(process.env.DATABASE_NAME)
        .collection(process.env.USER_COLLECTION);

    questionCollection = mongoClient.db(process.env.DATABASE_NAME)
        .collection(process.env.QUESTION_COLLECTION);

    inviteCollection = mongoClient.db(process.env.DATABASE_NAME)
        .collection(process.env.INVITE_COLLECTION);

    // set up indices if they are not there.
    await userCollection.createIndex({email: 1});

    await questionCollection.createIndex({trackerId: 1, expiryDate: 1});
    await questionCollection.createIndex({trackerId: 1});

    await inviteCollection.createIndex({questionTrackerId: 1, answered: 1});
    await inviteCollection.createIndex({trackerId: 1});
    await inviteCollection.createIndex({questionTrackerId: 1, trackerId: 1, targetUserEmail: 1});
    await inviteCollection.createIndex({answered: 1, targetUserEmail: 1});
    await inviteCollection.createIndex({answered: 1, trackerId: 1, expiryDate: 1});
    await inviteCollection.createIndex({targetUserEmail: 1, answered: 1, nextInviteTrackId: 1, expiryDate: 1});

}