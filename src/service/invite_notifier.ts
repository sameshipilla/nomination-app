import {Invite} from "../entities/Invite";
import {inviteCollection} from "../init";
import {randomBytes} from "crypto";
import {add, getReInvitableOnlineUsers} from "./online";
import * as http from "http";
import { Server } from 'socket.io';
import {getPendingInvites} from "./invite";

let io;
export function initNotifier(server: http.Server){
    io = new Server(server);
    io.on('connection', (socket) => {
        socket.on('ping', async (email) => {
            await add(email);
        })
    });
}

function notifyAll(invite: Invite){
    setImmediate(async () => {
        // get all the invites for the target user email and emit
        // the pending invites.
        const allInvites = await getPendingInvites(invite.targetUserEmail);
        const eventType = `invites-${invite.targetUserEmail}`;
        io.emit(eventType, allInvites);
    });
}

function handleInviteTimeout(invite: Invite){
    setTimeout(async () => {
        // if no online users, don't bother forwarding the invite.
        const onlineUsers = await getReInvitableOnlineUsers(invite, invite.targetUserEmail);
        if(onlineUsers.length === 0)
            return;

        const targetInvites = await inviteCollection.aggregate(
            [
                {
                    $match: {
                        nextInviteTrackId: null,
                        trackerId: invite.trackerId
                    }
                },
                {
                    $lookup: {
                        from: process.env.QUESTION_COLLECTION,
                        localField: 'questionTrackerId',
                        foreignField: 'trackerId',
                        as: 'question'
                    }
                },
                {
                    $unwind: '$question'
                },
                {
                    $match: {
                        "question.expiryDate": {$gte: new Date()}
                    }
                }
            ]).toArray();


        // if invite has been forwarded or game is ended, just return.
        // Invite cannot be forwarded to the next user.
        if(targetInvites.length === 0)
            return;

        const targetUser = onlineUsers[Math.floor(Math.random() * onlineUsers.length)];

        const newInvite: Invite = {
            questionTrackerId: invite.questionTrackerId,
            trackerId: randomBytes(100).toString('hex'),
            targetUserEmail: targetUser.email,
        }

        await inviteCollection.updateOne({trackerId: invite.trackerId}, {$set: {nextInviteTrackId: newInvite.trackerId}});
        await inviteCollection.insertOne(newInvite);
        inviteNotifier.notifyAll(newInvite);
        // notify so that the old invite can been removed.
        inviteNotifier.notifyAll(invite);
        await inviteNotifier.handleInviteTimeout(newInvite);
    }, 1000 * 60 * 3);
}

export const inviteNotifier = {
    notifyAll, handleInviteTimeout
}