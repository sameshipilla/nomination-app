import {getAsync, inviteCollection, questionCollection, userCollection} from "../init";
import moment from "moment";
import {InviteResult} from "../entities/InviteResult";
import {Invite} from "../entities/Invite";
import {Question} from "../entities/Question";
import {User} from "../entities/User";
import {getOnlineUsers, getReInvitableOnlineUsers} from "./online";
import {randomBytes} from "crypto";
import {inviteNotifier} from "./invite_notifier";

export async function getPendingInvites(email: string): Promise<any[]>{
    const pendingInvites = await inviteCollection.aggregate([
        {
            $match: {
                targetUserEmail: email,
                answered: {$ne: true},
                nextInviteTrackId: null,
                $or: [{expiryDate: {$gte: new Date()}}, {expiryDate: null}],
            }
        },
        {
            $lookup: {
                from: process.env.QUESTION_COLLECTION,
                localField: 'questionTrackerId',
                foreignField: 'trackerId',
                as: 'question'
            }
        },
        {$unwind: '$question'},
        {
            $lookup: {
                from: process.env.USER_COLLECTION,
                localField: 'question.initiatorEmail',
                foreignField: 'email',
                as: 'initiator'
            }
        },
        {$unwind: '$initiator'},
        {
            $match: {'question.expiryDate': {$gt: new Date()}}
        }
    ]).toArray();

    return pendingInvites.map(it => {
        const expiryDate = it['question'].expiryDate;
        it['remainingTime'] = moment(expiryDate).diff(new Date(), 'minutes');
        return it;
    });
}

export async function acceptInvite(trackerId: string): Promise<InviteResult>{
    if(!trackerId){
        throw "Invalid Invite link";
    }

    const inviteFilter = {
        trackerId,
        answered: {$ne: true},
        nextInviteTrackId: null,
        $or: [{expiryDate: null}, {expiryDate: {$gte: new Date()}}]
    };
    const invite: Invite = await inviteCollection.findOne(inviteFilter);

    if(!invite){
        throw "Invite has expired, has been answered already or has been forwarded to another user.";
    }

    const questionFilter = {
        trackerId: invite.questionTrackerId,
        expiryDate: {$gte: new Date()}
    }

    const targetQuestion: Question = await questionCollection.findOne(questionFilter);
    if(!targetQuestion){
        throw "The game has already ended";
    }

    if(!invite.expiryDate){
        const expiryDate = moment().add(targetQuestion.questionTimer, 'seconds').toDate();
        await inviteCollection.updateOne({trackerId}, {$set: {expiryDate}});
        invite.expiryDate = expiryDate;
    }

    await inviteCollection.updateOne({trackerId}, {$set: {attempted: true}});

    const questionRemainingTime = moment(invite.expiryDate).diff(new Date(), 'seconds');
    const gameRemainingTime = moment(targetQuestion.expiryDate).diff(new Date(), 'minutes');

    return {
        gameRemainingTime,
        questionRemainingTime,
        targetQuestion,
    };
}

export async function getAnsweredInvites(targetQuestion: Question): Promise<Invite[]>{
    let answeredInvites = await inviteCollection.aggregate([
        {
            $match: {
                questionTrackerId: targetQuestion.trackerId,
                answered: true
            }
        },
        {$sort: {expiryDate: -1}},
        {
            $lookup: {
                from: process.env.USER_COLLECTION,
                localField: 'targetUserEmail',
                foreignField: 'email',
                as: 'user'
            }
        },
        {
            $unwind: '$user'
        }
    ]).toArray();

    return answeredInvites.map(it => {
        it['username'] = it.user.email.split('@')[0]
        return it;
    });
}

export async function renominateUser(inviteEmail: string, prevTrackerId: string, currentUserEmail: string): Promise<string>{
    if(!inviteEmail || !prevTrackerId)
        throw '/?message=Invalid URL'

    const prevInvite: Invite = await inviteCollection.findOne({trackerId: prevTrackerId});
    if(!prevInvite){
        throw '/?message=Invalid URL'
    }

    let targetUser: User;
    if(inviteEmail === 'random'){
        const users = await getReInvitableOnlineUsers(prevInvite, currentUserEmail);
        if(users.length === 0)
            throw '/?message=Could not find a random online user to nominate';

        targetUser = users[Math.floor(Math.random() * users.length)]
    } else {
        targetUser = await userCollection.findOne({email: inviteEmail});
        if(!targetUser){
            throw '/?message=Invalid URL'
        }
    }

    const hasUserAnsweredInvite = await inviteCollection.findOne({
        targetUserEmail: targetUser.email,
        answered: true,
        questionTrackerId: prevInvite.questionTrackerId
    });

    if(hasUserAnsweredInvite){
        throw `/renominate_online_users?prevTrackerId=${prevTrackerId}&message=User has already answered this question. Try again.`;
    }

    const newInvite: Invite = {
        questionTrackerId: prevInvite.questionTrackerId,
        trackerId: randomBytes(100).toString('hex'),
        targetUserEmail: targetUser.email,
    }

    await inviteCollection.updateOne({trackerId: prevTrackerId}, {$set: {nextInviteTrackId: newInvite.trackerId}});
    await inviteCollection.insertOne(newInvite);
    inviteNotifier.notifyAll(newInvite);
    inviteNotifier.handleInviteTimeout(newInvite);

    return `/?message=Sent invite to ${targetUser.name}&status=success`;
}

export async function answerQuestion(answer1: string, answer2: string, answer3: string, trackerId: string, questionsNum: number){

    if(!trackerId || !answer1){
        throw `/accept_invite?trackerId=${trackerId}&message=All fields are required`
    }

    if((!answer2 && questionsNum === 2) || (!answer3 && questionsNum == 3)){
        throw `/accept_invite?trackerId=${trackerId}&message=All fields are required`;
    }

    const inviteFilter = {
        trackerId,
        answered: {$ne: true},
        expiryDate: {$gte: new Date()}
    };

    const invite: Invite = await inviteCollection.findOne(inviteFilter);

    if(!invite){
        throw `/?message=Could not save question answer. The invite has expired already or is invalid`;
    }

    const questionFilter = {
        trackerId: invite.questionTrackerId,
        expiryDate: {$gte: new Date()}
    }

    const targetQuestion: Question = await questionCollection.findOne(questionFilter);
    if(!targetQuestion){
        throw `/?message=Could not submit the answer.The game has already ended`;
    }

    await inviteCollection.updateOne({trackerId}, {$set: {answered: true, answer1, answer2, answer3}});

    return `/renominate_user?prevTrackerId=${trackerId}`;
}

export async function nominateUser(qnToken: string, inviteEmail: string, currentUserEmail: string){
    if (!inviteEmail || !qnToken) {
        throw '/ask_question?message=All fields are required to submit a question.';
    }

    const targetQuestionData = await getAsync(qnToken);
    if(!targetQuestionData){
        throw '/ask_question?message=Could not send the question at the moment. Please try again.';
    }

    if (inviteEmail === currentUserEmail) {
        throw '/ask_question?message=You cannot send a question to yourself.';
    }

    let targetUser;
    if(inviteEmail === 'random'){
        const ids = await getOnlineUsers();
        const targetIndex = ids.indexOf(currentUserEmail);
        ids.splice(targetIndex, 1);

        if(ids.length === 0){
            // If there are no online users, then you cannot send a question to a user.
            throw `/ask_question?qnToken=${qnToken}&message=Could not send question as no random online user was found`
        }

        const randId = ids[Math.floor(Math.random() * ids.length)];
        targetUser = await userCollection.findOne({email: randId});

    } else {
        targetUser = await userCollection.findOne({email: inviteEmail});
    }

    const question: Question = JSON.parse(targetQuestionData);
    question.trackerId = randomBytes(100).toString('hex')
    question.expiryDate = moment().add(question.gameTimer, 'minutes').toDate()
    question.initiatorEmail = currentUserEmail;

    await questionCollection.insertOne(question);

    const invite: Invite = {
        questionTrackerId: question.trackerId,
        targetUserEmail: targetUser.email,
        trackerId: randomBytes(100).toString('hex')
    }

    await inviteCollection.insertOne(invite);
    inviteNotifier.notifyAll(invite);
    await inviteNotifier.handleInviteTimeout(invite);

    return `/ask_question?message=Invited ${targetUser.name} to answer your question&status=success`;
}

export async function getAnsweredQuestions(currentUserEmail: string){
    return await inviteCollection.aggregate([
        {
            $match: {
                targetUserEmail: currentUserEmail,
                answered: true
            }
        },
        {
            $lookup: {
                from: process.env.QUESTION_COLLECTION,
                localField: 'questionTrackerId',
                foreignField: 'trackerId',
                as: 'question'
            }
        },
        {$unwind: '$question'},
        {$sort: {'question.expiryDate': -1}},
        {$project: {question: 1, _id: 0}},
        {
            $addFields: {
                questionText: '$question.questionText',
                initiatorEmail: '$question.initiatorEmail',
                gameTimer: '$question.gameTimer',
                questionTimer: '$question.questionTimer',
                trackerId: '$question.trackerId'
            }
        },
        {$project: {question: 0}},
        {
            $lookup: {
                from: process.env.USER_COLLECTION,
                foreignField: 'email',
                localField: 'initiatorEmail',
                as: 'user'
            }
        },
        {$unwind: '$user'},
        {$limit: 10}
    ]).toArray();
}