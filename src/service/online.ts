import {RedisClient} from "redis";
import {promisify} from 'util';
import {Invite} from "../entities/Invite";
import {User} from "../entities/User";
import {inviteCollection, userCollection} from "../init";

let timeoutMap: Map<string, any>
let saddAsync;
let sremAsync;
let smembersAsync;

export function initOnline(redis: RedisClient){
    timeoutMap = new Map<string, any>();
    saddAsync = promisify(redis.sadd).bind(redis);
    sremAsync = promisify(redis.srem).bind(redis);
    smembersAsync = promisify(redis.smembers).bind(redis);
}

export async function add(username: string){
    const oldTimeoutId = timeoutMap.get(username);

    if(oldTimeoutId){
        return;
    }

    await saddAsync(process.env.ONLINE_USERS_KEY, username);

    const timeoutId = setTimeout(async () => {
        await sremAsync(process.env.ONLINE_USERS_KEY, username);
        timeoutMap.delete(username);
    }, 1000 * 10);

    timeoutMap.set(username, timeoutId);
}

export async function getOnlineUsers(): Promise<string[]>{
    return await smembersAsync(process.env.ONLINE_USERS_KEY);
}

export async function remove(username: string) {
    await sremAsync(process.env.ONLINE_USERS_KEY, username);
}

export async function getReInvitableOnlineUsers(prevInvite: Invite, currentUserEmail: string): Promise<User[]>{
    const onlineIds = await getOnlineUsers();
    onlineIds.splice(onlineIds.indexOf(currentUserEmail), 1);

    const answeredUsersFilter = {
        answered: true,
        questionTrackerId: prevInvite.questionTrackerId
    }

    const answeredUserIds = await inviteCollection.find(answeredUsersFilter).project({targetUserEmail: 1}).toArray();
    for (let userId of answeredUserIds){
        const targetEmail = userId['targetUserEmail']
        const targetIndex = onlineIds.indexOf(targetEmail)
        if(targetIndex !== -1){
            onlineIds.splice(targetIndex, 1)
        }
    }

    const users: User[] = await userCollection.find({email: {$in: onlineIds}}).toArray();
    return users.map(it => {
        it['username'] = it['email'].split('@')[0]
        return it;
    });
}