$(document).ready(() => {
    let remainingTime = $('#remainingTimeLabel').text();
    const trackerId = $('#trackerId').val();

    if(!remainingTime)
        return;

    const interval = setInterval(() => {
        if(remainingTime === 0){
            clearInterval(interval);
            alert('This question has timed out. Proceed to nominate another user.');
            window.location.href = `/renominate_user?prevTrackerId=${trackerId}`;
            return;
        }

        remainingTime--;
        $('#remainingTimeLabel').text(remainingTime + '');
    }, 1000);
});