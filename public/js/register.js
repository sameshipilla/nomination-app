function doRegister(){
    const name = $('#name').val();
    const email = $('#email').val();
    const password = $('#password').val();
    const confirmPassword = $('#confirmPassword').val();

    let formHasError = false;

    hideErrorFields();

    const doneCallback = () => {
        formHasError = true;
    };

    if(!name || name.trim().length < 2){
        setFieldErrorText('nameError', 'Name should be 2 characters or more', doneCallback);
    }

    if(!email){
        setFieldErrorText('emailError', 'Email is required', doneCallback);
    }

    if(email && !validateEmail(email)){
        setFieldErrorText('emailError', 'Invalid email provided', doneCallback);
    }


    if(!password){
        setFieldErrorText('passwordError', 'Password is required', doneCallback);
    }

    if(password && password.length < 6){
        setFieldErrorText('passwordError', 'Password should have 6 or more characters', doneCallback);
    }

    if(password !== confirmPassword){
        setFieldErrorText('confirmPasswordError', 'Passwords do not match', doneCallback);
    }

    if(!formHasError){
        $('#registerForm').submit();
    }
}

function hideErrorFields(){
    ['emailError', 'passwordError', 'confirmPasswordError', 'questionTimerError', 'gameTimerError',
        'questionTextError', 'nameError'].forEach(it => {
        $('#' + it).hide();
    });
}

function setFieldErrorText(errorTextId, errorMessage, doneCallback){
    $('#' + errorTextId).text(errorMessage).show();
    if(doneCallback){
        doneCallback();
    }
}

function validateEmail(email){
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
}

function back(){
    window.history.back();
}

$(document).ready(() => {
    hideErrorFields();
});
