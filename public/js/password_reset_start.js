function doPasswordResetStart(){
    const email = $('#email').val();

    let formHasError = false;

    hideErrorFields();

    const doneCallback = () => {
        formHasError = true;
    };

    if(!email){
        setFieldErrorText('emailError', 'Email is required', doneCallback);
    }

    if(!formHasError){
        $('#resetPasswordStartForm').submit();
    }
}