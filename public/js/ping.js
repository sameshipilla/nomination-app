$(document).ready(() => {
    const socket = io();
    setInterval(() => {
        const cookieStr = document.cookie;
        const currentUserCookie = cookieStr.split(';')
            .find(it => it.trim().includes('whomai'));

        if(!currentUserCookie){
            return;
        }

        let currentUserEmail = currentUserCookie.split('=')[1]
        currentUserEmail = decodeURIComponent(currentUserEmail);
        socket.emit('ping', currentUserEmail);
    }, 1000 * 2)
});