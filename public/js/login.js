function doLogin(){
    const email = $('#email').val();
    const password = $('#password').val();

    let formHasError = false;


    hideErrorFields();

    const doneCallback = () => {
        formHasError = true;
    };

    if(!email){
        setFieldErrorText('emailError', 'Email is required', doneCallback);
    }

    if(!password){
        setFieldErrorText('passwordError', 'Password is required', doneCallback);
    }

    if(!formHasError){
        $('#loginForm').submit();
    }
}