$(document).ready(() => {

    const page = $('#home-flag').attr('data-page');

    if (page !== 'home')
        return;

    const currentUserEmail  = $('#currentUserId').text();

    const socket = io();

    const invitesEvent = `invites-${currentUserEmail}`;
    socket.on(invitesEvent, (invites) => {
        const htmlData = invites.map(it => inviteToHTML(it)).join(' ');

        const invitesWrapper = $('#invitesWrapper');
        if(!invitesWrapper.text().trim()){
            $('#noInvitesWrapper').hide();
            invitesWrapper.html(getInvitesWrapperShell());
        }

        $('#pendingInvitesBody').html(htmlData);
    });
});

function getInvitesWrapperShell(){
    return `<div class="ml-5 mr-5 mt-3">
            <div class="d-flex justify-content-center">
                <img src="/img/logo.png" alt="Logo" style="width: 15rem;"/>
           </div>
            <h4>My Pending Questions</h4>
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Initiator</th>
                    <th>Question</th>
                    <th>Game Timeout</th>
                    <th>Question Timeout</th>
                    <th class="text-danger">Remaining Time</th>
                    <th class="text-center">Answer</th>
                </tr>
                </thead>
                <tbody id="pendingInvitesBody">
                </tbody>
            </table>
        </div>`;
}

function inviteToHTML(pendingInvite) {
    return `<tr>
               <td>${pendingInvite.initiator.name}</td>
               <td>${pendingInvite.question.questionText}</td>
               <td>${pendingInvite.question.gameTimer} minutes</td>
               <td>${pendingInvite.question.questionTimer} seconds</td>
               <td class="text-danger">${pendingInvite.remainingTime} minutes</td>
               <td class="text-center">
                   <a href="/accept_invite?trackerId=${pendingInvite.trackerId}" class="btn btn-primary text-white btn-sm">Answer</a>
               </td>
            </tr>`;
}