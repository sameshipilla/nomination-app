function doAskQuestion(){
    hideErrorFields();

    const questionText = $('#questionText').val();
    const questionTimer = +$('#questionTimer').val();
    const gameTimer = +$('#gameTimer').val();

    let formHasError = false;

    const doneCallback = () => {
        formHasError = true;
    };

    if(!questionText.trim()){
        setFieldErrorText('questionTextError', 'Question text is required', doneCallback);
    }

    if(questionText.trim().length > 255){
        setFieldErrorText('questionTextError', 'Question text cannot be greater than 255 characters', doneCallback);
    }

    if(questionTimer < 30 || questionTimer > 60){
        setFieldErrorText('questionTimerError', 'Question timer should be between 30 and 60', doneCallback);
    }

    if(gameTimer < 15 || gameTimer > 60){
        setFieldErrorText('gameTimerError', 'Question timer should be between 15 and 60', doneCallback);
    }

    if(formHasError)
        return;

    $('#askQnForm').submit();
}

$(document).ready(() => {
    localStorage.removeItem('current-question-temp');
});